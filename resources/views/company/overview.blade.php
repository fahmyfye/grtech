@extends('layout.main')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0">COMPANY</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{ Session::get('message')}}</strong>
                </div>
            @endif

            <div class="content-body">
                <section class="basic-elements">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col">
                                            <h4 class="card-title">COMPANY LISTING</h4>
                                        </div>
                                        <div class="col text-right">
                                            {{-- <button type="button" class="btn btn-primary btn-md mr-1 mb-1">New +</button> --}}
                                            <a href="{{ route('company.create') }}" class="btn btn-primary btn-md mr-1 mb-1">New +</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div  class="table-responsive">
                                            <table class="table table-striped table-bordered dom-jQuery-events">
                                                <thead>
                                                <tr>
                                                   <th>#</th>
                                                   <th>Name</th>
                                                   <th>Email</th>
                                                   <th>Logo</th>
                                                   <th>Website</th>
                                                   <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($data as $key => $company)
                                                <tr>
                                                   <td>{{ $key+1 }}</td>
                                                    <td class="width-300">{{ $company->name }}</td>
                                                    <td>{{ $company->email }}</td>
                                                    <td>
                                                        <img src="{{URL::asset($company->logo)}}" alt="profile Pic" height="100" width="100">
                                                   </td>
                                                    <td><a class="dropdown-item" href="{{ $company->website }}}" target="_blank">{{ $company->website }}</a></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn btn-outline-info block btn-md" data-toggle="modal" data-target="#editForm_{!! $company->id !!}">
                                                                    Edit
                                                                </button>

                                                                <!-- Modal -->
                                                                <div class="modal fade text-left" id="editForm_{!! $company->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <label class="modal-title text-text-bold-600" id="myModalLabel33">{!! $company->name !!}</label>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form name="formname" method="post" action="{{ route('company.update') }}" enctype="multipart/form-data" novalidate>
                                                                                @csrf
                                                                                @method('PUT')

                                                                                <div class="modal-body">
                                                                                    <label>Name: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="hidden" name="id" value="{{ $company->id}}">
                                                                                        <input type="text" name="name" class="form-control" value="{{ $company->name }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Email: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="email" class="form-control" value="{{ $company->email }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Website: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="website" class="form-control" value="{{ $company->website }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Logo: </label>
                                                                                    <div class="form-group">
                                                                                        @if (!empty($company->logo))
                                                                                            <img src="{{URL::asset($company->logo)}}" alt="profile Pic" height="100" width="100">
                                                                                        @endif
                                                                                        <br/>
                                                                                        <input type="file" name="logo" class="form-control" value="" >
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                                <div class="modal-footer">
                                                                                    <input type="reset" class="btn btn-outline-secondary btn-md" data-dismiss="modal" value="Close">
                                                                                    <input type="submit" class="btn btn-outline-primary btn-md" value="Update">
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <form method="POST" action="{{ route('company.destroy') }}">
                                                                    @csrf
                                                                    @method('DELETE')

                                                                    <input type="hidden" id="form-emp-id" name="id" value="{!! $company->id !!}">
                                                                    <input type="submit" onclick="return confirm('Delete Company?')" class="btn btn-outline-danger btn-md" value="Delete">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <!-- jQuery 3 -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <script>


        $(document).ready(function(){

           
        });


    </script>

    <script src="{{ asset('app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>


    <script src="{{ asset('app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>

    {{--<script src="{{ asset('app-assets/js/scripts/ui/breadcrumbs-with-stats.js') }}"></script>--}}
    <script src="{{ asset('app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
@endsection