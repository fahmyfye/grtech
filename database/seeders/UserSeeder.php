<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('passport:client --name=grtech --no-interaction --personal');

        User::insert([
            [
                'name'     => 'Admin',
                'email'    => 'admin@grtech.com.my',
                'password' => bcrypt('password'),
                'roles'    => 'Admin',
            ],
            [
                'name'     => 'User',
                'email'    => 'user@grtech.com.my',
                'password' => bcrypt('password'),
                'roles'    => 'User',
            ],
        ]);
    }
}
