<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {
        $data = Company::all();
        return view('company.overview', compact('data'));
    }

    public function view($id)
    {
        $data = Company::where('id', $id)->first();
        return view('company.view', compact('data'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'    => 'required|string',
                'email'   => 'required|string|email|unique:companies,email',
                'logo'    => 'file|image|mimes:jpeg,png,gif,webp|max:5120',
                'website' => 'required|string',
            ],
        );

        if ($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['status' => 'Error', 'message' => $error->all()], 400);
        } else {
            $path = $request->file('logo')->store('logo');

            $data = Company::create([
                'name'    => $request->name,
                'email'   => $request->email,
                'logo'    => $path,
                'website' => $request->website,
            ]);

            if ($data) {
                session()->flash('message', "Success");
                session()->flash('alert-class', 'alert-success');
            } else {
                session()->flash('message', "Error");
                session()->flash('alert-class', 'alert-danger');
            }

            return redirect()->route('company.index');
        }
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make(
            $request->all(),
            [
                'id'      => 'required|integer',
                'name'    => 'string',
                'email'   => 'string|email|unique:employees,email,' . $request->id,
                'logo'    => 'file|image|mimes:jpeg,png,gif,webp|max:5120',
                'website' => 'string',
            ],
        );

        if ($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['status' => 'Error', 'message' => $error->all()], 400);
        } else {
            if ($request->hasFile('logo')) {
                $path = $request->file('logo')->store('logo');
            }

            $start = Company::where('id', $request->id)->first();
            $data  = Company::where('id', $request->id)->update([
                'name'    => isset($request->name) ? $request->name : $start->name,
                'email'   => isset($request->email) ? $request->email : $start->email,
                'logo'    => $request->hasFile('logo') ? $path : $start->logo,
                'website' => isset($request->website) ? $request->website : $start->website,
            ]);

            $data = Company::where('id', $request->id)->first();

            if ($data) {
                session()->flash('message', "Success");
                session()->flash('alert-class', 'alert-success');
            } else {
                session()->flash('message', "Error");
                session()->flash('alert-class', 'alert-danger');
            }

            return redirect()->route('company.index');
        }
    }

    public function destroy(Request $request)
    {
        $data = Company::where('id', $request->id)->delete();
        if ($data) {
            session()->flash('message', "Success");
            session()->flash('alert-class', 'alert-success');
        } else {
            session()->flash('message', "Error");
            session()->flash('alert-class', 'alert-danger');
        }

        return redirect()->route('company.index');
    }
}
