<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth Start
Route::middleware('auth')->group(function () {
    Route::get('dashboard', [AuthController::class, 'login'])->name('dashboard');

    // Company
    Route::group(['prefix' => 'company'], function () {
        Route::get('/', [CompanyController::class, 'index'])->name('company.index');
        Route::get('/create', [CompanyController::class, 'create'])->name('company.create');
        Route::get('/{id}', [CompanyController::class, 'view'])->name('company.view');
        Route::post('/', [CompanyController::class, 'store'])->name('company.store');
        Route::put('/', [CompanyController::class, 'update'])->name('company.update');
        Route::delete('/', [CompanyController::class, 'destroy'])->name('company.destroy');
    });

    // Employee
    Route::group(['prefix' => 'employee'], function () {
        Route::get('/', [EmployeesController::class, 'index'])->name('employee.index');
        Route::get('/create', [EmployeesController::class, 'create'])->name('employee.create');
        Route::get('/{id}', [EmployeesController::class, 'view'])->name('employee.view');
        Route::post('/', [EmployeesController::class, 'store'])->name('employee.store');
        Route::put('/', [EmployeesController::class, 'update'])->name('employee.update');
        Route::delete('/', [EmployeesController::class, 'destroy'])->name('employee.destroy');
    });
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
