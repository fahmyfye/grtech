<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::insert([
            [
                'name'    => 'Twitternation',
                'email'   => 'moxbrough2@bing.com',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Blogpad',
                'email'   => 'dwickens5@archive.org',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Brainverse',
                'email'   => 'darnolda@illinois.edu',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Topdrive',
                'email'   => 'shilaryc@cam.ac.uk',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Linkbridge',
                'email'   => 'lsutherele@hubpages.com',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Shufflebeat',
                'email'   => 'alapsley1s@geocities.com',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Realbuzz',
                'email'   => 'mbigmore1p@japanpost.jp',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Photobean',
                'email'   => 'khuguet1m@blogger.com',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Devpulse',
                'email'   => 'yloynton1e@ask.com',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
            [
                'name'    => 'Livetube',
                'email'   => 'hgare13@friendfeed.com',
                'logo'    => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'website' => 'https://grtech.com/',
            ],
        ]);
    }
}
