{{--test--}}
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header"><span>General</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
            </li>
            <li class=" nav-item"><a href=""><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
            </li>
            <li class=" navigation-header"><span>Module</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Module"></i>
            </li>
            <li class=" nav-item"><a href="{{ route('company.index') }}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Company</span></a></li>
            <li class=" nav-item"><a href="{{ route('employee.index') }}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Employee</span></a></li>
            
            </li>
            <li class=" nav-item">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="ft-power"></i><span class="menu-title" data-i18n="">Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>