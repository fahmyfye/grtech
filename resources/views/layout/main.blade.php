<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>

    @include('layout._admin_header')

</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('layout._admin_navbar')

    @if(auth()->user()->roles  == 'Admin')
        @include('layout._admin_menu')
    @else
        @include('layout._user_menu')
    @endif

    @yield('content')

    @include('layout._admin_footer')

</body>
</html>