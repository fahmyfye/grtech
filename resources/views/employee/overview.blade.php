@extends('layout.main')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title mb-0">EMPLOYEE</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{ Session::get('message')}}</strong>
                </div>
            @endif

            <div class="content-body">
                <section class="basic-elements">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col">
                                            <h4 class="card-title">EMPLOYEE LISTING</h4>
                                        </div>
                                        {{-- <div class="col-6">

                                        </div> --}}
                                        <div class="col text-right">
                                            {{-- <button type="button" class="btn btn-primary btn-md mr-1 mb-1">New +</button> --}}
                                            <a href="{{ route('employee.create') }}" class="btn btn-primary btn-md mr-1 mb-1">New +</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div  class="table-responsive">
                                            <table class="table table-striped table-bordered dom-jQuery-events">
                                                <thead>
                                                <tr>
                                                   <th>#</th>
                                                   <th>Full Name</th>
                                                   <th>Email</th>
                                                   <th>Phone</th>
                                                   <th>Company</th>
                                                   <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($data as $key => $employee)
                                                <tr>
                                                   <td>{{ $key+1 }}</td>
                                                    <td class="width-300">{{ $employee->fullname }}</td>
                                                    <td>{{ $employee->email }}</td>
                                                    <td>{{ $employee->phone }}</td>
                                                    <td>
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-outline-warning block btn-md" data-toggle="modal" data-target="#inlineForm_{!! $employee->id !!}">
                                                            {{ $employee->company->name }}
                                                        </button>

                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="inlineForm_{!! $employee->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <label class="modal-title text-text-bold-600" id="myModalLabel33">{!! $employee->company->name !!}</label>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="#">
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <div class="modal-body">
                                                                                    <label>Email: </label>
                                                                                    <div class="form-group">
                                                                                        {!! $employee->company->email !!}
                                                                                    </div>

                                                                                    <label>Website: </label>
                                                                                    <div class="form-group">
                                                                                        <a href="{!! $employee->company->website !!}" target="_blank">{!! $employee->company->website !!}</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-6">
                                                                                <div class="modal-body">
                                                                                    <img src="{{URL::asset($employee->company->logo)}}" alt="profile Pic" height="200" width="200">
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <input type="reset" class="btn btn-outline-secondary btn-md" data-dismiss="modal" value="close">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn btn-outline-info block btn-md" data-toggle="modal" data-target="#editForm_{!! $employee->id !!}">
                                                                    Edit
                                                                </button>

                                                                <!-- Modal -->
                                                                <div class="modal fade text-left" id="editForm_{!! $employee->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <label class="modal-title text-text-bold-600" id="myModalLabel33">{!! $employee->company->name !!}</label>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form name="formname" method="post" action="{{ route('employee.update') }}" novalidate>
                                                                                @csrf
                                                                                @method('PUT')

                                                                                <div class="modal-body">
                                                                                    <label>First Name: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="hidden" name="id" value="{{ $employee->id}}">
                                                                                        <input type="text" name="first_name" class="form-control" value="{{ $employee->first_name }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Last Name: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="last_name" class="form-control" value="{{ $employee->last_name }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Email: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="email" class="form-control" value="{{ $employee->email }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Phone: </label>
                                                                                    <div class="form-group">
                                                                                        <input type="text" name="phone" class="form-control" value="{{ $employee->phone }}" required data-validation-required-message="This field is required">
                                                                                    </div>

                                                                                    <label>Company: </label>
                                                                                    <div class="form-group">
                                                                                        <select class="select2 form-control" name="company_id">
                                                                                            @foreach ($company as $value)
                                                                                                <option value="{!! $value->id !!}"
                                                                                                    @if ($value->id == $employee->company_id)
                                                                                                        selected
                                                                                                    @endif
                                                                                                    > {!! $value->name !!}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                                <div class="modal-footer">
                                                                                    <input type="reset" class="btn btn-outline-secondary btn-md" data-dismiss="modal" value="Close">
                                                                                    <input type="submit" class="btn btn-outline-primary btn-md" value="Update">
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <form method="POST" action="{{ route('employee.destroy') }}">
                                                                    @csrf
                                                                    @method('DELETE')

                                                                    <input type="hidden" id="form-emp-id" name="id" value="{!! $employee->id !!}">
                                                                    <input type="submit" onclick="return confirm('Delete Employee?')" class="btn btn-outline-danger btn-md" value="Delete">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    

    <!-- jQuery 3 -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <script>
        function deleteEmployee(id) {
            $('#form-emp-id').val(id)
            $('#form-emp-submit').click()
        }
    </script>

    <script src="{{ asset('app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>


    <script src="{{ asset('app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>

    {{--<script src="{{ asset('app-assets/js/scripts/ui/breadcrumbs-with-stats.js') }}"></script>--}}
    <script src="{{ asset('app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
@endsection