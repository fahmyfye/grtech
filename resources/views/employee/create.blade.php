
@extends('layout.main')

@section('content')

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
               <h3 class="content-header-title mb-0">EMPLOYEE</h3>
               <div class="row breadcrumbs-top">
                  <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                           </li>
                           <li class="breadcrumb-item"><a href="{{ route('employee.index') }}">Employee</a>
                           </li>
                        </ol>
                  </div>
               </div>
            </div>
      </div>
      <div class="content-body">
         <section class="basic-elements">
            <div class="row">
               <div class="col-md-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Create New Employee Form</h4>
                     </div>
                     <div class="card-content">
                        <div class="card-body">
                           <form name="formname" method="post" action="{{ route('employee.store') }}" novalidate>
                              @csrf
                              <div class="row">
                                 <div class="col-lg-6 col-md-12">
                                       <div class="form-group">
                                          <h5>First Name</h5>
                                          <div class="controls">
                                             <input type="text" name="first_name" class="form-control" value="" required data-validation-required-message="This field is required">
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <h5>Last Name</h5>
                                          <div class="controls">
                                             <input type="text" name="last_name" class="form-control" value="" required data-validation-required-message="This field is required">
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <h5>Email</h5>
                                          <div class="controls">
                                             <input type="text" name="email" class="form-control" value="" required data-validation-required-message="This field is required">
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <h5>Phone No.</h5>
                                          <div class="controls">
                                             <input type="text" name="phone" class="form-control" value="" required data-validation-required-message="This field is required">
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <h5>Company</h5>
                                          <div class="controls">
                                             <select class="select2 form-control" name="company_id">
                                                @foreach ($company as $value)
                                                   <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                                                @endforeach
                                            </select>
                                          </div>
                                       </div>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-lg-12 col-md-12">
                                       <div class="form-actions">
                                          <div class="text-left">
                                             <a href="{{ route('employee.index') }}" class="btn btn-blue-grey"><i class="ft-skip-back position-right"></i> Back</a>
                                             <button type="submit" class="btn btn-success">Save <i class="ft-save position-right"></i></button>
                                          </div>
                                       </div>
                                 </div>
                              </div>

                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>

<!-- jQuery 3 -->
<script src="{{ asset('admin/bower_components/jquery/dist/jquery.min.js') }}"></script>

<script src="{{ asset('admin/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>

<script src="{{ asset('admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/picker.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/pickadate/legacy.js') }}"></script>
<script src="{{ asset('admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>

<script type="text/javascript" src="{{ asset('admin/app-assets/js/scripts/ui/breadcrumbs-with-stats.js') }}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.js') }}"></script>
<script src="{{ asset('admin/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
@endsection