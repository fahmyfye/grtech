<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\Employees;
use App\Models\Company;
use App\Mail\GRTechMail;

class EmployeesController extends Controller
{
    public function index()
    {
        $data = Employees::with('company')->get();
        $company = Company::all();
        return view('employee.overview', compact('data', 'company'));
    }

    public function view($id)
    {
        $data    = Employees::where('id', $id)->with('company')->first();
        $company = Company::all();
        return view('employee.view', compact('data', 'company'));
    }

    public function create()
    {
        $company = Company::all();
        return view('employee.create', compact('company'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required|string',
                'last_name'  => 'required|string',
                'email'      => 'required|string|email|unique:employees,email',
                'phone'      => 'required|string',
                'company_id' => 'required|integer',
            ]
        );

        if ($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['status' => 'Error', 'message' => $error->all()], 400);
        } else {
            $data = Employees::create([
                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'email'      => $request->email,
                'phone'      => $request->phone,
                'company_id' => $request->company_id,
            ]);

            if ($data) {
                session()->flash('message', "Success");
                session()->flash('alert-class', 'alert-success');

                $details = [
                    'title' => 'New Employee',
                    'body'  => 'New Employee Registered:' . $request->first_name . ' ' . $request->last_name . ' : ' . $request->email,
                ];

                Mail::to('syafiq@grtech.com.my')->send(new GRTechMail($details));
            } else {
                session()->flash('message', "Error");
                session()->flash('alert-class', 'alert-danger');
            }

            return redirect()->route('employee.index');
        }
    }

    public function update(Request $request, Employees $employees)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id'         => 'required|integer',
                'first_name' => 'required|string',
                'last_name'  => 'required|string',
                'email'      => 'required|email|unique:employees,email,' . $request->id,
                'phone'      => 'required|string',
                'company_id' => 'required|integer',
            ],
        );

        if ($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['status' => 'Error', 'message' => $error->all()], 400);
        } else {
            $start = Employees::where('id', $request->id)->first();
            $data  = Employees::where('id', $request->id)->update([
                'first_name' => isset($request->first_name) ? $request->first_name : $start->first_name,
                'last_name'  => isset($request->last_name) ? $request->last_name : $start->last_name,
                'email'      => isset($request->email) ? $request->email : $start->email,
                'phone'      => isset($request->phone) ? $request->phone : $start->phone,
                'company_id' => isset($request->company_id) ? $request->company_id : $start->company_id,
            ]);

            $data = Employees::where('id', $request->id)->first();

            if ($data) {
                session()->flash('message', "Success");
                session()->flash('alert-class', 'alert-success');
            } else {
                session()->flash('message', "Error");
                session()->flash('alert-class', 'alert-danger');
            }

            return redirect()->route('employee.index');
        }
    }

    public function destroy(Request $request)
    {
        $data = Employees::where('id', $request->id)->delete();
        if ($data) {
            session()->flash('message', "Success");
            session()->flash('alert-class', 'alert-success');
        } else {
            session()->flash('message', "Error");
            session()->flash('alert-class', 'alert-danger');
        }

        return redirect()->route('employee.index');
    }
}
